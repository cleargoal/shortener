@extends('layouts.app')

@section('content')

    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                Laravel URL shorten
            </div>
            <div class="sub-title m-b-md">
                The best way is to register at us so you'll have possibility to see all your links and control them
            </div>

            @if(!empty($error))
                <div class="link-error">
                    <h1>{{$error}}</h1>
                </div>
            @elseif(!empty($expired))
                <div class="expired">
                    <h1>Link already expired</h1>
                </div>
            @else
                <div>
                    @include('includes.shorten_form')
                </div>
            @endif

            <div class="saved-link">
                @if(!empty($savedLink))
                    <h4>Your short link:</h4>
                    <p class="badge-secondary">{{$savedLink}}</p>
                @endif
            </div>
        </div>
    </div>
@endsection
