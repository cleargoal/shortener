@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        You are logged in!
                    </div>
                    <div class="card-body">
                        <h3>Your links:</h3>

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Full link</th>
                                <th scope="col">Short link</th>
                                <th scope="col">Expiration</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($myLinks as $key => $myLink)
                                <tr @if(!empty($myLink->expired) && $myLink->expired < date('Y-m-d H:i:s')) class="expired" @endif>
                                    <th scope="row">{{$key+1}}</th>
                                    <td style="word-break: break-word;">{{$myLink->full_link}}</td>
                                    <td>{{route('welcome') . '/' . $myLink->short_link}}</td>
                                    <td>{{$myLink->expired}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mt-4">
                    <a href="{{route('welcome')}}" class="btn btn-outline-info">Add more...</a>
                </div>
            </div>
        </div>
    </div>
@endsection
