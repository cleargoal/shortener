<form method="POST" action="{{route('savelink')}}" class="just-form">
    @csrf
    <label for="full_link">Put here your link you want to shorten</label>
    <input type="text" name="full_link" id="full_link" placeholder="Put here your link you want to shorten" autofocus
           required>
    <div>
        <label for="check">Link expiration:</label>
        <input type="checkbox" id="check">
        <label for="expired">Link expires in minutes</label>
        <input type="number" name="expired" id="expired" min="1" max="1000" step="1" disabled>
    </div>
    <input type="submit" class="btn btn-primary submit">
</form>
@push('scripts')
    <script>
        const checkbox = document.querySelector('#check');
        const expired = document.querySelector('#expired');
        checkbox.addEventListener('change', function (event) {
            expired.disabled = !event.target.checked;
        }, false);
    </script>
@endpush
