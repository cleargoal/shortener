<?php

namespace App\Http\Controllers;

use App\ShortLink;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ShortLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $randomLink = '';
        while(true) {
            $randomLink = Str::random(5);
            if (ShortLink::where('short_link', $randomLink)->first() === null) {
                break;
            }
        }
        $savedLink = new ShortLink();
        $savedLink->user_id = Auth::user() ? Auth::user()->id : null;
        $savedLink->full_link = $request->full_link;
        $savedLink->short_link = $randomLink;
        $savedLink->expired = !empty($request->expired) ? (new Carbon())->addMinutes($request->expired) : null;
        $savedLink->save();
        return redirect()->route('linksaved', ['savedLink' => route('welcome') . '/' . $savedLink->short_link]);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        return view('/welcome')->with(['savedLink' => !empty($request->savedLink) ? $request->savedLink : '']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\ShortLink $shortLink
     * @return \Illuminate\Http\Response
     */
    public function edit(ShortLink $shortLink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param \App\ShortLink $shortLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShortLink $shortLink)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\ShortLink $shortLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShortLink $shortLink)
    {
        //
    }

    public function goToFullUrl($shortUrl)
    {
        $getFullUrl = ShortLink::where('short_link', $shortUrl)->first();
        if (empty($getFullUrl)) {
            return view('welcome', ['error' => 'There is an error in provided link']);
        }
        if($getFullUrl->expired !== null && $getFullUrl->expired < Carbon::now()->format('Y-m-d H:i:s')) {
            return view('welcome', ['expired' => true]);
        }
        return redirect()->away($getFullUrl->full_link);
    }
}
