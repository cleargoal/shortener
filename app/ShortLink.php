<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShortLink extends Model
{
    public $timestamps;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'full_link', 'short_link', 'expired',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
